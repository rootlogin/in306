# Test-Projekt für verteilte System

## Features
 * Mini-Blogging Tool auf Basis Schulbeispiel
 * Twitter Bootstrap 4
 * Quote of the Day
 * [Docker-Hub](https://hub.docker.com/r/rootlogin/in306)
 * Auto-build via [Gitlab CI](https://gitlab.com/rootlogin/in306/blob/master/.gitlab-ci.yml)
 * API-Schnittstelle

## URLs (via Docker-Compose)

* Blog: [localhost:8080/](http://localhost:8080/)
* OpenAPI-UI: [http://localhost:8080/api/openapi-ui](http://localhost:8080/api/openapi-ui)

### Api-Methoden

#### JWT Token beziehen

 * **Methode**: POST
 * **Pfad**: /api/token

**JSON Request Body:**
```
{
    "username": "string",
    "password": "string"
}
```

**HTTPie Beispiel:**
```
$ http POST localhost:8080/api/token username=admin password=admin

HTTP/1.1 200 OK
Content-Length: 136
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJibG9nYXBwIiwidXNlciI6ImFkbWluIn0.WqzaA0BKtMbCwJRQK8iM2JALHTH7KIIK36p1zUlTt8Y"
}
```

#### Alle Blogposts abrufen

 * **Methode**: GET
 * **Pfad**: /api/posts

**HTTPie Beispiel:**
```
$ http GET localhost:8080/api/posts
HTTP/1.1 200 OK
Content-Length: 373
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

[
    {
        "content": "Woah, ein Blog mit JavaEE!",
        "title": "Mein erster Eintrag!"
    },
    {
        "content": "Woah, Java EE macht so Spass!",
        "title": "Zweiter Eintrag"
    },
    {
        "content": "Diese Bloggerei geht voll ab!",
        "title": "Noch ein letzter Blogeintrag!"
    }
]
```

#### Blogpost erstellen

 * **Methode**: POST
 * **Pfad**: /api/posts
 * **Authorisierung**: JWT Bearer Token

**JSON Request Body:**
```
{
    "title": "string",
    "content": "string"
}
```

**HTTPie Beispiel:**
```
$ http POST localhost:8080/api/posts 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJibG9nYXBwIiwidXNlciI6ImFkbWluIn0.WqzaA0BKtMbCwJRQK8iM2JALHTH7KIIK36p1zUlTt8Y' title='Ein schöner Tag' content='Heute ist ein schöner Tag'
HTTP/1.1 200 OK
Content-Length: 33
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

{
    "id": 201,
    "uri": "/api/posts/201"
}
```

#### Blogpost abrufen

 * **Methode**: GET
 * **Pfad**: /api/posts/{id}

**HTTPie Beispiel:**
```
$ http GET localhost:8080/api/posts/1
HTTP/1.1 200 OK
Content-Length: 71
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

{
    "content": "Woah, ein Blog mit JavaEE!",
    "title": "Mein erster Eintrag!"
}
```

#### Kommentare abrufen

 * **Methode**: GET
 * **Pfad**: /api/posts/{id}/comments

```
$ http GET localhost:8080/api/posts/1/comments
HTTP/1.1 200 OK
Content-Length: 133
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

[
    {
        "content": "Ey, voll krass!",
        "lastUpdated": "10.03.2020 08:36"
    },
    {
        "content": "Das gibt sicher 100%!",
        "lastUpdated": "10.03.2020 08:36"
    }
]
```

#### Blogpost bearbeiten

 * **Methode**: PUT
 * **Pfad**: /api/posts/{id}
 * **Authorisierung**: JWT Bearer Token

**JSON Request Body:**
```
{
    "title": "string",
    "content": "string"
}
```

**HTTPie Beispiel:**
```
$ http PUT localhost:8080/api/posts/201 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJibG9nYXBwIiwidXNlciI6ImFkbWluIn0.WqzaA0BKtMbCwJRQK8iM2JALHTH7KIIK36p1zUlTt8Y' title='Ein schöner Tag' content='Heute ist der schönste Tag'
HTTP/1.1 200 OK
Content-Length: 68
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

{
    "content": "Heute ist der schönste Tag",
    "title": "Ein schöner Tag"
}
```

#### Blogpost löschen

 * **Methode**: DELETE
 * **Pfad**: /api/posts/{id}
 * **Authorisierung**: JWT Bearer Token

**HTTPie Beispiel:**
```
$ http DELETE localhost:8080/api/posts/201 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJibG9nYXBwIiwidXNlciI6ImFkbWluIn0.WqzaA0BKtMbCwJRQK8iM2JALHTH7KIIK36p1zUlTt8Y'
HTTP/1.1 200 OK
Content-Length: 27
Content-Type: application/json
Server: Payara Server  5.194 #badassfish
X-Frame-Options: SAMEORIGIN
X-Powered-By: Servlet/4.0 JSP/2.3 (Payara Server  5.194 #badassfish Java/Azul Systems, Inc./1.8)

{
    "message": "Post deleted!"
}
```

## Konfiguration

Die Konfiguration der Anwendung findet über Environment-Variablen statt:

 * **DB_USERNAME**: Datenbank Benutzername
 * **DB_PASSWORD**: Datenbank Passwort
 * **DB_URL**: Datenbank JDBC URL (z.B. DB_URL=mysql://db-host/db-name)
 
Die Konfigurationsparameter können entweder in der "docker-compose.yml" oder manuell via "docker run -e DB_URL=mysql://db-host/db-name rootlogin/in306" übergeben werden.

## Standardbenutzer

Bei der Initialisierung der Datenbank wird automatisch der Benutzer "admin" mit dem Passwort "admin" erstellt.

## Build/Start der Applikation mit docker-compose

**Vorraussetzungen:**

 * funktionierende docker-compose Installation
 * Port 8080 und 4848 dürfen nicht belegt sein.
 
```shell script
# Von docker hub laden (optional)
docker pull rootlogin/in306

# Applikation builden (optional)
docker-compose build

# Applikation starten
docker-compose up -d

# Applikation stoppen
docker-compose stop
```

Anschliessend kann die Blogapplikation nach wenigen Sekunden unter [localhost:8080/blog](http://localhost:8080/blog/index.xhtml) aufgerufen werden.

## Tipps und Tricks

 * Zum Schnellen rebuilden und neustarten, kann das Script `./rebuild.sh` unter Linux verwendet werden.