#!/usr/bin/env bash

if [ -z "${DB_URL}" ]; then
  echo "Please set env variable DB_URL"
  exit 1;
fi

if [ -z "${DB_USERNAME}" ]; then
  echo "Please set env variable DB_USERNAME"
  exit 1;
fi

if [ -z "${DB_PASSWORD}" ]; then
  echo "Please set env variable DB_PASSWORD"
  exit 1;
fi

# reset postboot commands
rm -f "${POSTBOOT_COMMANDS}"
touch "${POSTBOOT_COMMANDS}"

cat > ${POSTBOOT_COMMANDS} <<EOL
create-jdbc-connection-pool --driverclassname com.mysql.jdbc.Driver --restype java.sql.Driver --property useSSL=false:password=${DB_PASSWORD}:user=${DB_USERNAME}:url="jdbc:${DB_URL}" AppPool
create-jdbc-resource --connectionpoolid AppPool jdbc/blogData
EOL

exec ${SCRIPT_DIR}/entrypoint.sh