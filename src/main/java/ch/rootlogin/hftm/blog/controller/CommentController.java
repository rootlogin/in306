package ch.rootlogin.hftm.blog.controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ch.rootlogin.hftm.blog.models.Comment;
import ch.rootlogin.hftm.blog.models.Post;
import ch.rootlogin.hftm.blog.models.repo.CommentRepository;

@Named
@RequestScoped
public class CommentController extends BaseController
{
	@Inject
	private CommentRepository repo;

	@NotNull(message = "Gib bitte einen Kommentar ein!")
	@Size(min = 5, message = "Ein Kommentar hat mind. 5 Zeichen!")
	private String content;

	public String getContent()
	{
		return content;
	}

	public void setContent(final String content)
	{
		this.content = content;
	}

	public String save(final Post p)
	{
		repo.persist(p.getId(), new Comment(getContent()));
		addMessage(FacesMessage.SEVERITY_INFO, "Der Kommentar wurde gespeichert!");
		return redirect(PostController.POST_PAGE + "?id=" + p.getId());
	}
}
