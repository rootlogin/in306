package ch.rootlogin.hftm.blog.util;

import java.util.ArrayList;
import java.util.List;

public class Helper {
    public static <T> List<T> makeList(Iterable<T> iter) {
        ArrayList<T> list = new ArrayList<T>();
        for (T item : iter) {
            list.add(item);
        }
        return list;
    }
}
