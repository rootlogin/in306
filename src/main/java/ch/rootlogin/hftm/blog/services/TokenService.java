package ch.rootlogin.hftm.blog.services;

import ch.rootlogin.hftm.blog.models.ApiToken;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;

import javax.enterprise.context.ApplicationScoped;
import java.util.logging.Logger;

@ApplicationScoped
public class TokenService {
    private final String SECRET = "HFTMandJAVAEEforWINNERS";
    private final String ISSUER = "blogapp";

    private final static Logger LOGGER = Logger.getLogger(TokenService.class.getName());

    public ApiToken getToken(String username) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);

            String token =  JWT.create()
                    .withIssuer(ISSUER)
                    .withClaim("user", username)
                    .sign(algorithm);

            return new ApiToken(token);
        } catch (JWTCreationException ex) {
            LOGGER.warning(String.format("Couldn't create token: %s", ex.getMessage()));
        }

        return null;
    }

    public boolean verifyToken(String token) {
        if(token == null) {
            return false;
        }

        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISSUER)
                    .build(); //Reusable verifier instance

            verifier.verify(token);
            return true;
        } catch (JWTVerificationException ex){
            LOGGER.info(String.format("Error verifying token: %s", ex.getMessage()));
            return false;
        }
    }
}
