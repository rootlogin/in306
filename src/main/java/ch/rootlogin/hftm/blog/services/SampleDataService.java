package ch.rootlogin.hftm.blog.services;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import ch.rootlogin.hftm.blog.models.Comment;
import ch.rootlogin.hftm.blog.models.Post;
import ch.rootlogin.hftm.blog.models.User;

@ApplicationScoped
public class SampleDataService
{
	@PersistenceContext
	private EntityManager em;

	@Transactional
	void addPosts(@Observes @Initialized(ApplicationScoped.class) final Object event) {
		try {
			if (em.createQuery("SELECT COUNT(p) FROM Post p", Long.class).getSingleResult() == 0) {
				final Post first = new Post("Mein erster Eintrag!", "Woah, ein Blog mit JavaEE!");
				first.addComment(new Comment("Ey, voll krass!"));
				first.addComment(new Comment("Das gibt sicher 100%!"));
				em.persist(first);
				final Post second = new Post("Zweiter Eintrag", "Woah, Java EE macht so Spass!");
				second.addComment(new Comment("Wow, ey, stimm ich voll zu!"));
				em.persist(second);
				em.persist(new Post("Noch ein letzter Blogeintrag!", "Diese Bloggerei geht voll ab!"));
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	@Transactional
	void addUsers(@Observes @Initialized(ApplicationScoped.class) final Object event) {
		try {
			if (em.createQuery("SELECT COUNT(u) FROM User u", Long.class).getSingleResult() == 0) {
				em.persist(new User("Administrator", "admin", "admin"));
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

}
