package ch.rootlogin.hftm.blog.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Post extends CreatableEntity {
	@Id
	@GeneratedValue
	private Long id;

	private String title;
	private String content;

	@OneToMany(mappedBy = "post", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Comment> comments = new ArrayList<>();

	protected Post() {
	}

	public Post(final ApiPost apiPost) {
		this.title = apiPost.getTitle();
		this.content = apiPost.getContent();
	}

	public Post(final Long id, final String title, final String content) {
		this.id = id;
		this.title = title;
		this.content = content;
	}

	public Post(final String title, final String content) {
		this.title = title;
		this.content = content;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void addComment(final Comment comment) {
		comments.add(comment);
		comment.setPost(this);
	}

	public Iterable<Comment> getComments() {
		return comments;
	}

	public boolean hasComments() {
		return !comments.isEmpty();
	}

	public Optional<Comment> getLatestComment() {
		if (comments.isEmpty()) {
			return Optional.empty();
		}
		return comments.stream().max((c1, c2) -> c1.getCreatedAt().compareTo(c2.getCreatedAt()));
	}

	@Override
	public String toString() {
		return "Post " + getId() + ", title " + getTitle();
	}
}
