package ch.rootlogin.hftm.blog.models;

import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class CreatableEntity {

	@Column
	@JsonbDateFormat("dd.MM.yyyy hh:mm")
	private LocalDateTime createdAt;

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(final LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	@PreUpdate
	@PrePersist
	public void updateCreatedAt() {
		final LocalDateTime now = LocalDateTime.now();
		if (getCreatedAt() == null) {
			setCreatedAt(now);
		}
	}
}
