package ch.rootlogin.hftm.blog.models;

public class ApiToken {
	private String token;

	protected ApiToken() {}

	public ApiToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
