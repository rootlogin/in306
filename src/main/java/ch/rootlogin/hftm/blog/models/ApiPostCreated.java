package ch.rootlogin.hftm.blog.models;

import java.net.URI;

public class ApiPostCreated {
	private int id;
	private String uri;

	public ApiPostCreated() {}

	public ApiPostCreated(final int id, final String uri) {
		this.id = id;
		this.uri = uri;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}