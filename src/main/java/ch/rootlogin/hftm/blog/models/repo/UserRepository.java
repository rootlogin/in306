package ch.rootlogin.hftm.blog.models.repo;

import ch.rootlogin.hftm.blog.models.User;

import javax.enterprise.context.RequestScoped;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;
import java.util.logging.Logger;

@RequestScoped
public class UserRepository {

	@PersistenceContext
	private EntityManager em;

	public Iterable<User> findAll() {
		return em.createQuery("SELECT u FROM User u", User.class).getResultList();
	}

	public Optional<User> find(final Long id) {
		return Optional.ofNullable(em.find(User.class, id));
	}

	public Optional<User> findByUsername(final String username) {
		TypedQuery<User> q = em.createQuery("SELECT u FROM User u WHERE u.username = :username", User.class);
		q.setParameter("username", username);

		try {
			return Optional.ofNullable(q.getSingleResult());
		} catch(NoResultException ex) {
			return Optional.empty();
		}
	}
}
