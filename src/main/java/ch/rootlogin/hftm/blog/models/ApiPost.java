package ch.rootlogin.hftm.blog.models;

import javax.json.bind.annotation.JsonbDateFormat;
import java.time.LocalDateTime;

public class ApiPost {
	private String title;
	private String content;

	@JsonbDateFormat("dd.MM.yyyy hh:mm")
	private LocalDateTime lastUpdated;

	public ApiPost() {}

	public ApiPost(Post post) {
		this.title = post.getTitle();
		this.content = post.getContent();
		this.lastUpdated = post.getCreatedAt();
	}

	public ApiPost(final String title, final String content) {
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}