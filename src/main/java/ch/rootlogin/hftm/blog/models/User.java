package ch.rootlogin.hftm.blog.models;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.logging.Logger;

@Entity
public class User extends CreatableEntity {
	private final static Logger LOGGER = Logger.getLogger(User.class.getName());

	@Id
	@GeneratedValue
	@JsonbTransient
	private long id;

	private String displayname;

	@Column(unique=true)
	private String username;

	@JsonbTransient
	private String passwordHash;

	protected User() {
	}

	public User(final String displayname, final String username, final String password) {
		this.displayname = displayname;
		this.username = username;

		try {
			setPassword(password);
		} catch(NoSuchAlgorithmException | InvalidKeySpecException ex) {
			LOGGER.severe(ex.getMessage());
		}
	}

	public long getId() {
		return id;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);

		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

		byte[] hash = factory.generateSecret(spec).getEncoded();
		this.passwordHash = Hex.encodeHexString(salt) + ":" + Hex.encodeHexString(hash);
	}

	public boolean comparePassword(String password) {
		if(password == null) {
			return false;
		}

		try {
			String[] passwordHashSplitted = this.passwordHash.split(":");

			byte[] salt = Hex.decodeHex(passwordHashSplitted[0]);
			byte[] passwordHash = Hex.decodeHex(passwordHashSplitted[1]);

			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

			byte[] newHash = factory.generateSecret(spec).getEncoded();

			if(Arrays.equals(passwordHash, newHash)) {
				return true;
			}
		} catch(NoSuchAlgorithmException | InvalidKeySpecException | DecoderException ex) {
			LOGGER.severe(ex.getMessage());
		}

		return false;
	}
}
