package ch.rootlogin.hftm.blog.models;

import javax.json.bind.annotation.JsonbDateFormat;
import java.time.LocalDateTime;

public class ApiComment {
	private String content;

	@JsonbDateFormat("dd.MM.yyyy hh:mm")
	private LocalDateTime lastUpdated;

	public ApiComment() {}

	public ApiComment(final String content, final LocalDateTime lastUpdated) {
		this.content = content;
		this.lastUpdated = lastUpdated;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}