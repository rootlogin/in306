package ch.rootlogin.hftm.blog.models;

public class ApiMessage {
	private String message;

	public ApiMessage() {}

	public ApiMessage(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}