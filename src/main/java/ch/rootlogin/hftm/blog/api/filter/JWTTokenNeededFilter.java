package ch.rootlogin.hftm.blog.api.filter;

import ch.rootlogin.hftm.blog.services.TokenService;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@JWTTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {

    @Inject
    TokenService tokenService;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        // Get the HTTP Authorization header from the request
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if(authorizationHeader != null) {
            String token = authorizationHeader.substring("Bearer".length()).trim();

            if(tokenService.verifyToken(token)) {
                return;
            }
        }

        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
    }
}