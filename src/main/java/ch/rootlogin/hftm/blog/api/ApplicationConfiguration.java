package ch.rootlogin.hftm.blog.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

@ApplicationPath("api")
@OpenAPIDefinition(
        info = @Info(
                title = "Blog application",
                version = "0.1"
        ),
        servers = {
                @Server(url = "/", description = "localhost")
        })
@SecurityScheme(securitySchemeName = "jwt", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "jwt")
public class ApplicationConfiguration extends Application {}