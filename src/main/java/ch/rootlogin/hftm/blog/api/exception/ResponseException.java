package ch.rootlogin.hftm.blog.api.exception;

import ch.rootlogin.hftm.blog.models.ApiMessage;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ResponseException extends WebApplicationException {
    private static final long serialVersionUID = 1L;

    public ResponseException(int status, String message) {
        super(message, Response.status(status).header("info", message).entity(new ApiMessage(message)).build());
    }
}
