package ch.rootlogin.hftm.blog.api;

import ch.rootlogin.hftm.blog.api.exception.ResponseException;
import ch.rootlogin.hftm.blog.models.ApiLogin;
import ch.rootlogin.hftm.blog.models.ApiPostCreated;
import ch.rootlogin.hftm.blog.models.ApiToken;
import ch.rootlogin.hftm.blog.models.User;
import ch.rootlogin.hftm.blog.models.repo.UserRepository;
import ch.rootlogin.hftm.blog.services.TokenService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/token")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Dependent
public class TokenResource {

    @Inject
    private TokenService tokenService;

    @Inject
    private UserRepository userRepository;

    @POST @Path("/")
    @Operation(description = "Get JWT token")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Successful, returning the value"),
            @APIResponse(responseCode = "403", description = "Wrong password")
    })
    @RequestBody(content = @Content(schema = @Schema(implementation = ApiLogin.class)), required = true)
    public ApiToken getToken(ApiLogin login) {
        if(login == null) {
            throw new ResponseException(400, "Empty request!");
        }

        Optional<User> oUser = userRepository.findByUsername(login.getUsername());
        if(oUser.isPresent()) {
            User user = oUser.get();

            if(user.comparePassword(login.getPassword())) {
                return tokenService.getToken(user.getUsername());
            }
        }

        throw new ResponseException(403, "Wrong user or password!");
    }
}