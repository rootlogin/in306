package ch.rootlogin.hftm.blog.api;

import ch.rootlogin.hftm.blog.api.exception.ResponseException;
import ch.rootlogin.hftm.blog.api.filter.JWTTokenNeeded;
import ch.rootlogin.hftm.blog.models.*;
import ch.rootlogin.hftm.blog.models.repo.PostRepository;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("/posts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Dependent
public class BlogResource {
    @Inject
    private PostRepository repo;

    @GET @Path("/")
    @Operation(description = "List all posts")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Successful, returning the value")
    })
    public List<ApiPost> getAllPosts() {
        ArrayList<ApiPost> posts = new ArrayList<>();

        for (Post p : repo.findAll()) {
            posts.add(
                    new ApiPost(p)
            );
        }

        return posts;
    }

    @GET @Path("/{id}")
    @Operation(description = "Get one post by Id")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Successful, returning the value"),
            @APIResponse(responseCode = "404", description = "No post with this id available")
    })
    public ApiPost getPost(
            @PathParam("id") Long id
    ) {
        Optional<Post> post = repo.find(id);
        if (post.isPresent()) {
            return new ApiPost(post.get());
        } else {
            throw new ResponseException(404, "No post with this Id available.");
        }
    }

    @GET @Path("/{id}/comments")
    @Operation(description = "List all comments of post")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Successful, returning the value"),
            @APIResponse(responseCode = "404", description = "No post with this id available")
    })
    public List<ApiComment> getPostComments(
            @PathParam("id") Long id
    ) {
        Optional<Post> post = repo.find(id);
        if (post.isPresent()) {
            ArrayList<ApiComment> comments = new ArrayList<>();

            for(Comment c : post.get().getComments()) {
                comments.add(new ApiComment(c.getContent(), c.getCreatedAt()));
            }

            return comments;
        } else {
            throw new ResponseException(404, "No post with this Id available.");
        }
    }

    @POST @Path("/")
    @Operation(description = "Create post")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Successful, return post id")
    })
    @RequestBody(content = @Content(schema = @Schema(implementation = ApiPost.class)), required = true)
    @JWTTokenNeeded
    @SecurityRequirement(name = "jwt", scopes = {})
    public ApiPostCreated createPost(ApiPost aPost) {
        int id = repo.save(new Post(aPost)).getId().intValue();
        String postUri = String.format("/api/posts/%d", id);

        return new ApiPostCreated(id, postUri);
    }

    @PUT @Path("/{id}")
    @Operation(description = "Update post")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Successful, return post"),
            @APIResponse(responseCode = "404", description = "No post with this id available")
    })
    @RequestBody(content = @Content(schema = @Schema(implementation = ApiPost.class)), required = true)
    @JWTTokenNeeded
    @SecurityRequirement(name = "jwt", scopes = {})
    public ApiPost updatePost(
            @PathParam("id") int id,
            ApiPost aPost
    ) {
        Optional<Post> oPost = repo.find((long) id);
        if (oPost.isPresent()) {
            Post post = oPost.get();

            post.setTitle(aPost.getTitle());
            post.setContent(aPost.getContent());

            repo.save(post);
        } else {
            throw new ResponseException(404, "No post with this Id available.");
        }

        return aPost;
    }

    @DELETE @Path("/{id}")
    @Operation(description = "Delete post")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Post deleted"),
            @APIResponse(responseCode = "404", description = "No post with this id available")
    })
    @JWTTokenNeeded
    @SecurityRequirement(name = "jwt", scopes = {})
    public ApiMessage updatePost(
            @PathParam("id") int id
    ) {
        Optional<Post> oPost = repo.find((long) id);
        if (oPost.isPresent()) {
            Post post = oPost.get();

            repo.delete(post);
        } else {
            throw new ResponseException(404, "No post with this id available.");
        }

        return new ApiMessage("Post deleted!");
    }
}