FROM maven:3-jdk-8 as builder

RUN mkdir /tmp/build

COPY src/ /tmp/build/src/
COPY pom.xml /tmp/build

WORKDIR /tmp/build

RUN mvn package

FROM payara/server-full

# Privacy ;)
RUN rm $PAYARA_DIR/glassfish/modules/phonehome-bootstrap.jar

# Mysql-Treiber
RUN wget -O $PAYARA_DIR/glassfish/domains/production/lib/database-connector.jar https://jcenter.bintray.com/mysql/mysql-connector-java/5.1.48/mysql-connector-java-5.1.48.jar

# WAR-File deployen
COPY --from=builder /tmp/build/target/blog.war $DEPLOY_DIR/blog.war
COPY docker-entrypoint.sh /docker-entrypoint.sh

# Modify docker entrypoint
CMD ["/docker-entrypoint.sh"]